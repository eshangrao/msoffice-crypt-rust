use msoffice_crypt::{encrypt,decrypt};
fn main() {
        let input = "/home/feiy/Desktop/1.xlsx";
        let output = "/home/feiy/Desktop/output.xlsx";
        let password = "test";
        let ret = encrypt(input,password,Some(output));
        println!("{ret:#?}");
        let plain = "/home/feiy/Desktop/plain.xlsx";
        let ret = decrypt(output,password,Some(plain));
        println!("{ret:#?}");
        let plain = "/home/feiy/Desktop/plain.xlsx";
        let ret = encrypt(plain,password,None);
        println!("{ret:#?}");
        
}
