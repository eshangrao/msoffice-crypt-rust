# msoffice crypt lib

[msoffice-crypt](https://github.com/herumi/msoffice) bindings for the Rust programming language.

A lib to encrypt/decrypt Microsoft Office Document

## requirement

you must add the lib libmsoc.so to the system lib before run the bin app.

1.linux:

[libmsoc.so](https://gitee.com/eshangrao/msoffice-crypt-rust/tree/master/lib)

[you must compile the lib youself , read more](https://github.com/herumi/msoffice)

```bash
export LD_LIBRARY_PATH=some/path/to/libmsoc.so/dir:$LD_LIBRARY_PATH
```

2.windows:

[msoc.dll](https://gitee.com/eshangrao/msoffice-crypt-rust/tree/master/lib)

```bash
set PATH=some\path\to\libmsoc.dll\dir;%PATH%
```
## example

```rust
use msoffice_crypt::{encrypt,decrypt};
fn main() {
        # encrypt the input to output file
        let input = "/home/feiy/Desktop/1.xlsx";
        let output = "/home/feiy/Desktop/output.xlsx";
        let password = "test";
        let ret = encrypt(input,password,Some(output));
        println!("{ret:#?}");
        # decrypt the input file to output file
        let plain = "/home/feiy/Desktop/plain.xlsx";
        let ret = decrypt(output,password,Some(plain));
        println!("{ret:#?}");
        // overwrite the input file
        let plain = "/home/feiy/Desktop/plain.xlsx";
        let ret = encrypt(plain,password,None);
        println!("{ret:#?}");
        
}
```